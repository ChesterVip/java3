package pl.cansoft;

import java.util.Scanner;

public class CalculatorFull {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Jaki typ operacji chciałbyś wykonać?");
        System.out.println("A - dodawanie");
        System.out.println("B - odejmowanie");
        System.out.println("C - mnożenie");
        System.out.println("D - dzielenie");
        System.out.print("Wpisz opcję: ");

        String type;
        try {
            type = scan.nextLine();
            switch (type) {
                case "A":
                    add();
                    break;
                case "B":
                    minus();
                    break;
                case "C":
                    multiply();
                    break;
                case "D":
                    divide();
                    break;
                default:
                    throw new Exception();
            }
        } catch (Exception exception) {
            System.out.println("Typ operacji nie został rozpoznany.");
            main(args);
            return;
        }
    }

    static void add() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj argument 1: ");
        float firstArg;
        try {
            firstArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            add();
            return;
        }

        System.out.print("Podaj argument 2: ");
        float secondArg;
        try {
            secondArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            add();
            return;
        }

        float result = firstArg + secondArg;

        System.out.print("Wynik działania to: ");
        System.out.println(result);
    }

    static void minus() {
        // todo: należy zaimplementować opcję
    }

    static void multiply() {
        // todo: należy zaimplementować opcję
    }

    static void divide() {
        // todo: należy zaimplementować opcję
    }
}
