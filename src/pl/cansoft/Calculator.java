package pl.cansoft;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj argument 1:");
        float firstArg = 0;
        try {
            firstArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            main(args);
            return;
        }

        System.out.println("Podaj argument 2:");
        float secondArg = 0;
        try {
            secondArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            main(args);
            return;
        }

        float result = add(firstArg, secondArg);

        System.out.print("Wynik działania to: ");
        System.out.println(result);

        boolean isBigRes = isBiggerThanOneHundred(result);
        if (isBigRes) {
            System.out.print("Wartość jest większa niż 100");
        } else {
            System.out.print("Wartość jest mniejsza/równa niż 100");
        }
    }

    static float add(float argument1, float argument2) {
        return argument1 + argument2;
    }

    static boolean isBiggerThanOneHundred(float someNumber) {
        return someNumber > 100;
    }
}
